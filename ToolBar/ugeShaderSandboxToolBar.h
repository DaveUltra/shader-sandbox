#pragma once

#include <Win32/Window/Component/ugComponentW32.h>

class ugeShaderSandbox;

class ugButtonW32;
class ugComboBoxW32;

	/// The toolbar, located above the code editor.
	/// The toolbar contains buttons and controls to recompile the current shader, search symbols, etc...
	/// More tools are to be added in the future.
	///
	/// Sizes of widgets are set upon construction, but not the position. setSize() and setPosition() must
	/// be called systematically, one after another, after creating the toolbar.
class ugeShaderSandboxToolBar : public ugComponentW32
{
public:

	ugeShaderSandboxToolBar( ugeShaderSandbox* owner );

		/// Overriden methods to update the buttons positions too.
	void setPosition( const ugVector2& pos );
	void setSize( const ugVector2& size );



private:

	ugButtonW32* btn_runShader;

	ugComboBoxW32* combo_shaderLanguages;
};
#include "Editor/ugEditor.h"
#include "ugeShaderSandboxToolBar.h"

#include <Win32/Window/Component/ugComponentHeaderW32.h>

ugeShaderSandboxToolBar::ugeShaderSandboxToolBar( ugeShaderSandbox* owner )
	: ugComponentW32( (ugWindowW32*) owner, ugComponentType::TYPE_UNKNOWN )
{
	btn_runShader = new ugButtonW32( (ugWindowW32*) owner, ugButtonType::NORMAL, "" );
	{
		btn_runShader->setSize( ugVector2( 20, 20 ) );
	}

	combo_shaderLanguages = new ugComboBoxW32( (ugWindowW32*) owner, ugVector2( 100, 60 ) );
	{
		combo_shaderLanguages->addItem( "HLSL (sm 3.0)" );

		combo_shaderLanguages->setSelectedItemIndex( 0 );
	}
}

void ugeShaderSandboxToolBar::setPosition( const ugVector2& pos )
{
	ugComponentW32::setPosition( pos );

	combo_shaderLanguages->setPosition( pos );

	btn_runShader->setPosition( ugVector2( pos.x + 120, pos.y ) );
}

void ugeShaderSandboxToolBar::setSize( const ugVector2& size )
{
	ugComponentW32::setSize( size );
}
#include "Editor/ugEditor.h"
#include "ugeShaderSandboxMenuBar.h"

#include "Editor/Windows/ShaderSandbox/ugeShaderSandbox.h"

#include <Common/Base/System/Io/File/ugFile.h>

#include <Graphics/Dx9/DisplayContext/uggDisplayContextDX9.h>

#include <Win32/ugMessageBoxW32.h>
#include <Win32/Dialog/ugDialogW32.h>
#include <Win32/Window/Component/DirectXViewport/ugDirectXViewportW32.h>

ugeShaderSandboxMenuBar::ugeShaderSandboxMenuBar() : ugMenuW32( 0, 0 )
{
	ugMenuW32* file = new ugMenuW32( "File", this );
	{
		file->addItem( "New shader\tCtrl+N", menu_newShader );
		file->addItem( "Open shader\tCtrl+O", menu_openShader );
		file->addItem( "Save shader\tCtrl+S", menu_saveShader );
		file->addItem( "Save shader as\tCtrl+Shift+S", menu_saveShaderAs );

		file->addSeparator();

		file->addItem( "Exit", menu_exit );

		this->addSubmenu( file );
	}
}



void ugeShaderSandboxMenuBar::menu_newShader( ugMenuW32* menu, ugMenuItemW32* item )
{

}

void ugeShaderSandboxMenuBar::menu_openShader( ugMenuW32* menu, ugMenuItemW32* item )
{

}

void ugeShaderSandboxMenuBar::menu_saveShader( ugMenuW32* menu, ugMenuItemW32* item )
{

}

void ugeShaderSandboxMenuBar::menu_saveShaderAs( ugMenuW32* menu, ugMenuItemW32* item )
{

}

void ugeShaderSandboxMenuBar::menu_exit( ugMenuW32* menu, ugMenuItemW32* item )
{

}
#pragma once

#include <Win32/Window/Menu/ugMenuW32.h>

class ugeShaderSandboxMenuBar : public ugMenuW32
{
public:

	ugeShaderSandboxMenuBar();



private:

		///
		/// Events.
		///

		/// File.
	static void menu_newShader( ugMenuW32* menu, ugMenuItemW32* item );
	static void menu_openShader( ugMenuW32* menu, ugMenuItemW32* item );
	static void menu_saveShader( ugMenuW32* menu, ugMenuItemW32* item );
	static void menu_saveShaderAs( ugMenuW32* menu, ugMenuItemW32* item );

	static void menu_exit( ugMenuW32* menu, ugMenuItemW32* item );
};
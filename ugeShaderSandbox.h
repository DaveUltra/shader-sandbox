#pragma once

#include <Win32/Window/ugWindowW32.h>

class ugeShaderSandboxCodeEdit;
class ugeShaderSandboxViewport;
class ugeShaderSandboxToolBar;

	/// The main shader sandbox window.
class ugeShaderSandbox : public ugWindowW32
{
public:

	ugeShaderSandbox();

	virtual void onResize() override;

	virtual bool peekMessages() override;



private:

	ugeShaderSandboxCodeEdit* m_codeEdit;
	ugeShaderSandboxViewport* m_viewport;
	ugeShaderSandboxToolBar*  m_toolbar;
};
#pragma once

#include <Win32/Window/Component/DirectXViewport/ugDirectXViewportW32.h>

class ugeShaderSandbox;
class ugeShaderSandboxGUI;

class ugMesh;

	/// The viewport widget of the main shader sandbox window.
	/// The HUD is directly associated with this class, because there is no need to call
	/// its methods manually. The viewport's job is to take care of all graphics display
	/// plus camera controls and FPS estimation.
class ugeShaderSandboxViewport : public ugDirectXViewportW32
{
public:

	ugeShaderSandboxViewport( ugeShaderSandbox* owner, const ugVector2& size );

	void stepAndRender();

		/// Sets the displayed mesh by an index. The indices are :
		/// - 0 : plane
		/// - 1 : cube
		/// - 2 : sphere
		/// - 3 : cylinder
		/// The method will quit without changes if any other index is given.
	void setSelectedMesh( int meshIndex );



private:

	ugRotation m_cameraRotation;

		/// The HUD, displaying FPS and other useful information.
	ugeShaderSandboxGUI* m_hud;

	ugMesh* m_selectedMesh;
	ugMesh* m_meshes[4];
};
#include "Editor/ugEditor.h"
#include "ugeShaderSandboxGUI.h"

#include "Editor/Windows/ShaderSandbox/Viewport/ugeShaderSandboxViewport.h"

#include <Graphics/Common/Gui/uggGuiHeader.h>
#include <Graphics/Common/Data/Texture/uggTexture.h>
#include <Graphics/Common/Data/Font/uggFont.h>

ugeShaderSandboxGUI::ugeShaderSandboxGUI( ugeShaderSandboxViewport* viewport )
	: uggGui( (uggDisplayContext*) viewport->getContext() )
	, m_viewport( viewport )
{
	// FPS text.
	text_fps = new uggText( this, "FPS : 0", uggFont::load( "font:arial.ttf" ), 16 );
	{
		text_fps->setPosition( ugVector2( 4, 4 ) );
		text_fps->setColor( ugVector4( 1, 1, 0 ) );
	}

	// Buttons.
	btn_plane = new uggButton( this );
	{
		btn_plane->setAnchor( ANCHOR_BOTTOM_LEFT );
		btn_plane->setSize( ugVector2( 32, 32 ) );
		btn_plane->setPosition( ugVector2( 4, -4 ) );
		btn_plane->setTexture( uggTexture::load( "tex:shs_plane.png" ) );
	}

	btn_cube = new uggButton( this );
	{
		btn_cube->setAnchor( ANCHOR_BOTTOM_LEFT );
		btn_cube->setSize( ugVector2( 32, 32 ) );
		btn_cube->setPosition( ugVector2( 40, -4 ) );
		btn_cube->setTexture( uggTexture::load( "tex:shs_cube.png" ) );
	}

	btn_sphere = new uggButton( this );
	{
		btn_sphere->setAnchor( ANCHOR_BOTTOM_LEFT );
		btn_sphere->setSize( ugVector2( 32, 32 ) );
		btn_sphere->setPosition( ugVector2( 76, -4 ) );
		btn_sphere->setTexture( uggTexture::load( "tex:shs_sphere.png" ) );
	}

	btn_cylinder = new uggButton( this );
	{
		btn_cylinder->setAnchor( ANCHOR_BOTTOM_LEFT );
		btn_cylinder->setSize( ugVector2( 32, 32 ) );
		btn_cylinder->setPosition( ugVector2( 112, -4 ) );
		btn_cylinder->setTexture( uggTexture::load( "tex:shs_cylinder.png" ) );
	}
}

void ugeShaderSandboxGUI::onButtonClicked( uggButton* b )
{
	if ( b == btn_plane )
		m_viewport->setSelectedMesh( 0 );
	else if ( b == btn_cube )
		m_viewport->setSelectedMesh( 1 );
	else if ( b == btn_sphere )
		m_viewport->setSelectedMesh( 2 );
	else if ( b == btn_cylinder )
		m_viewport->setSelectedMesh( 3 );
}

void ugeShaderSandboxGUI::setFPSDisplay( int fps )
{
	text_fps->m_text = ugString( "FPS : " ) + fps;
}
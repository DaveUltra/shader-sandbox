#pragma once

#include <Graphics/Common/Gui/uggGui.h>

class ugeShaderSandboxViewport;

class uggText;
class uggButton;

	/// The main shader sandbox window's HUD, managed by the viewport.
	///
	/// The HUD must display FPS and other information on the shader,
	/// and provide a set of buttons to change the mesh to test the shader on.
class ugeShaderSandboxGUI : public uggGui
{
public:

	ugeShaderSandboxGUI( ugeShaderSandboxViewport* viewport );

		/// Mesh-switching button handling.
	virtual void onButtonClicked( uggButton* b ) override;

		/// Update FPS display (called by the viewport every second).
	void setFPSDisplay( int fps );



private:

		/// Owning viewport.
	ugeShaderSandboxViewport* m_viewport;

		/// FPS display, at the top-left corner.
	uggText* text_fps;

		/// Buttons, at the bottom left.
	uggButton* btn_plane;
	uggButton* btn_cube;
	uggButton* btn_sphere;
	uggButton* btn_cylinder;
};
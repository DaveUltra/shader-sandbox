#include "Editor/ugEditor.h"
#include "ugeShaderSandboxViewport.h"

#include "Editor/Windows/ShaderSandbox/Viewport/ugeShaderSandboxGUI.h"

#include <Common/Base/World/Entity/Camera/ugCamera.h>
#include <Common/Base/Input/Mouse/ugMouse.h>
#include <Common/Base/Input/Keyboard/ugKeyboard.h>

#include <Common/Base/Data/Mesh/Convex/Plane/ugPlaneMesh.h>
#include <Common/Base/Data/Mesh/Convex/Box/ugBoxMesh.h>
#include <Common/Base/Data/Mesh/Convex/Sphere/ugSphereMesh.h>
#include <Common/Base/Data/Mesh/Convex/Cylinder/ugCylinderMesh.h>

#include <Graphics/Dx9/DisplayContext/uggDisplayContextDX9.h>
#include <Graphics/Dx9/Renderer/Debugging/uggGridUtilDX9.h>

#include <Win32/Window/ugWindowW32.h>

ugeShaderSandboxViewport::ugeShaderSandboxViewport( ugeShaderSandbox* owner, const ugVector2& size )
	: ugDirectXViewportW32( (ugWindowW32*) owner, size )
{
	// No need to destroy the GUI, it will be destroyed with the viewport.
	this->getContext()->openGui( new ugeShaderSandboxGUI( this ), 0 );

	ugCamera* c = this->getContext()->getCamera();
	c->m_hasTarget = true;
	c->m_targetPos = ugVector4();
	c->setPosition( ugVector4( 1, 1, 1 ).normalize() * 2.0F );
	m_cameraRotation = ugRotation( 225, 45 );

	c->m_size = ugVector2( 4, 3 );



	m_meshes[0] = ugPlaneMesh::generate( "mesh:shs_plane", ugVector4( 0.5F, 0.5F, 0.5F ), 16 );
	m_meshes[1] = ugBoxMesh::generate( "mesh:shs_cube" );
	m_meshes[2] = ugSphereMesh::generate( "mesh:shs_sphere", 0.5F, 8 );
	m_meshes[3] = ugCylinderMesh::generate( "mesh:shs_cylinder", 0.5F, 1.0F, 16 );

	// Default selected mesh.
	m_selectedMesh = m_meshes[0];
}

void ugeShaderSandboxViewport::stepAndRender()
{
	ugCamera* c = this->getContext()->getCamera();

	// Camera controls.
	if ( ugKeyboard::isKeyDown( KEY_LEFT ) )  m_cameraRotation.m_yaw   += 1.0F;
	if ( ugKeyboard::isKeyDown( KEY_RIGHT ) ) m_cameraRotation.m_yaw   -= 1.0F;
	if ( ugKeyboard::isKeyDown( KEY_UP ) )    m_cameraRotation.m_pitch += 1.0F;
	if ( ugKeyboard::isKeyDown( KEY_DOWN ) )  m_cameraRotation.m_pitch -= 1.0F;

	ug_clamp( m_cameraRotation.m_pitch, -89.9F, 89.9F );

	c->setPosition( m_cameraRotation.getForwardVector() * 2.0F );

	/*if ( this->isMouseHovering() && ugMouse::isButtonDown( MOUSE_LEFTBUTTON ) )
	{
		this->getOwner()->captureMouse( true );

		ugRotation r = c->getRotation();
		r.m_yaw += ugMouse::getDX() * 0.3333333F;
		c->setPosition( r.getForwardVector() * 2.0F );
	}
	else
	{
		this->getOwner()->captureMouse( false );
	}*/



	//
	// Render.
	//

	this->beginDraw();

	this->getContext()->projection( c );
	this->getContext()->lookAt( c );

	this->getContext()->identity();
	uggGridUtilDX9::renderDebugGrid( this->getContext(), 4, 4, 1 );

	// TODO : bind shader.
	this->getContext()->getRenderer()->renderMesh( m_selectedMesh );

	this->endDraw();
}

void ugeShaderSandboxViewport::setSelectedMesh( int meshIndex )
{
	if ( meshIndex < 0 || meshIndex >= 4 )
		return;

	m_selectedMesh = m_meshes[meshIndex];
}
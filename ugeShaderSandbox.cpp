#include "Editor/ugEditor.h"
#include "ugeShaderSandbox.h"

#include "Editor/Windows/ShaderSandbox/CodeEdit/ugeShaderSandboxCodeEdit.h"
#include "Editor/Windows/ShaderSandbox/Viewport/ugeShaderSandboxViewport.h"
#include "Editor/Windows/ShaderSandbox/ToolBar/ugeShaderSandboxToolBar.h"

#include "Editor/Windows/ShaderSandbox/MenuBar/ugeShaderSandboxMenuBar.h"

ugeShaderSandbox::ugeShaderSandbox()
	: ugWindowW32( 1200, 620, "UGE Editor - Shader Sandbox" )
{
	m_codeEdit = 0;
	this->initialize();

	this->setMenu( new ugeShaderSandboxMenuBar() );

	m_codeEdit = new ugeShaderSandboxCodeEdit( this );
	{
		m_codeEdit->setPosition( ugVector2( 0, 0 ) );
	}

	m_viewport = new ugeShaderSandboxViewport( this, ugVector2( 400, 300 ) );
	{
		
	}

	m_toolbar = new ugeShaderSandboxToolBar( this );
	{
		
	}
}

void ugeShaderSandbox::onResize()
{
	if ( m_codeEdit )
	{
		ugFloat32 verticalSplit = 2.0F / 3.0F;

		m_codeEdit->setPosition( ugVector2( 0, 20 ) );
		m_codeEdit->setSize( ugVector2( m_width * verticalSplit, m_height - 20 ) );

		m_toolbar->setSize( ugVector2( m_codeEdit->getSize().x, 20 ) );
		m_toolbar->setPosition( ugVector2( 0, 0 ) );

		m_viewport->resizeViewport( ugVector2( m_width * verticalSplit, 0 ), ugVector2( m_width * (1.0F - verticalSplit), m_height * 0.5F ) );
	}
}

bool ugeShaderSandbox::peekMessages()
{
	ugWindowW32::peekMessages();

	if ( this->isFocused() )
	{
		m_viewport->stepAndRender();
	}

	return true;
}
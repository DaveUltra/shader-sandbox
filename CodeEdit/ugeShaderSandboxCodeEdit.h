#pragma once

#include <Win32/Window/Component/RichEdit/ugRichEditW32.h>

class ugeShaderSandbox;

class ugeShaderSandboxCodeEdit : public ugRichEditW32
{
public:

	ugeShaderSandboxCodeEdit( ugeShaderSandbox* owner );
};
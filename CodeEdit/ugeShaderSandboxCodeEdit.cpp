#include "Editor/ugEditor.h"
#include "ugeShaderSandboxCodeEdit.h"

const char* shs_hlsl_basicCode =

	// Vertex shader input.
	"struct VS_INPUT\n"
	"{\n"
	"\tfloat4 pos : POSITION0;\n"
	"\tfloat3 normal : NORMAL0;\n"
	"\tfloat2 texCoords : TEXCOORD0;\n"
	"};\n\n"

	// Vertex shader output / Pixel shader input.
	"struct VS_OUTPUT\n"
	"{\n"
	"\tfloat4 pos : POSITION0;\n"
	"\tfloat3 normal : NORMAL0;\n"
	"\tfloat2 texCoords : TEXCOORD0;\n"
	"};\n\n"

	// Pixel shader output (GBuffer).
	"struct PS_OUTPUT\n"
	"{\n"
	"\tfloat4 albedo : COLOR0;\n"
	"\tfloat4 normal : COLOR1;\n"
	"\tfloat4 depth : COLOR2;\n"
	"};\n\n\n\n"



	"uniform float4x4 world;\n"
	"uniform float4x4 view;\n"
	"uniform float4x4 projection;\n\n"

	"uniform bool isTex;\n"
	"sampler2D tex : register(s0);\n\n\n\n"



	"// Vertex shader entry point.\n"
	"VS_OUTPUT vs_main( VS_INPUT input )\n"
	"{\n"
	"\tVS_OUTPUT output;\n\n"

	"\toutput.pos = mul( input.pos, world );\n"
	"\toutput.pos = mul( output.pos, view );\n"
	"\toutput.pos = mul( output.pos, projection );\n\n"

	"\toutput.normal = mul( input.normal, (float3x3) world );\n\n"

	"\toutput.texCoords = input.texCoords;\n\n"

	"\treturn output;\n"
	"}\n\n"

	"// Pixel shader entry point.\n"
	"PS_OUTPUT ps_main( VS_OUTPUT input )\n"
	"{\n"
	"\tPS_OUTPUT output;\n\n"

	"\toutput.albedo = 1;\n"
	"\tif ( isTex ) output.albedo *= tex2D( tex, input.texCoords );\n\n"

	"\treturn output;\n"
	"}\n";

ugeShaderSandboxCodeEdit::ugeShaderSandboxCodeEdit( ugeShaderSandbox* owner )
	: ugRichEditW32( (ugWindowW32*) owner )
{
	this->setText( (char*) shs_hlsl_basicCode );
}
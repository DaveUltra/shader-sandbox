UGE Editor - ShaderSandbox

(c) 2021 David Kroft.

---

Description : The ShaderSandbox tool was reworked and adapted from an old standalone tool of the same name.
The new version is now part of ugEditor.

A version of ugEditor with only the ShaderSandbox tool will be provided in this repo for testing.

---

/!\ THE CODE IN THIS REPOSITIORY WAS MADE PUBLIC ONLY FOR SHOWCASE AND SHOULD NOT BE COPIED
OR MODIFIED WITHOUT THE PERMISSION OF THE ORIGINAL AUTHOR.
